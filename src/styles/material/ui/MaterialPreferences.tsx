import { Configuration } from "app/config/user/Configuration";
import { PrimitiveSetting, Setting } from "app/config/user/Setting";
import { UVUIPreferences } from "app/ui/elements/UVUIPreferences";
import { h } from "tsx-dom";
import MaterialTabBar from "./components/MaterialTabBar";
import MaterialPreferencesTab from "./MaterialPreferencesTab";
import "../css/preferences.css";
import Log from "app/data/AppLog";
import MaterialButton from "./components/MaterialButton";
import UltravioletUI from "app/ui/UltravioletUI";
import i18next from "i18next";

/**
 * The MaterialPreferences is a handling class used for the preferences page.
 */
export default class MaterialPreferences extends UVUIPreferences {
    /**
     * Unsaved preferences cache
     */
    private unsavedPreferences: PrimitiveSetting<any>[] = [];

    /**
     * Handler for onChange events.
     */
    onChange(setting: PrimitiveSetting<any>): void {
        // if exists in unsaved preferences, remove it
        Log.trace("MaterialPreferences onChange called", { setting });
        const index = this.unsavedPreferences.findIndex(
            (pref) => pref.id === setting.id
        );
        if (index !== -1) {
            this.unsavedPreferences.splice(index, 1);
        }
        this.unsavedPreferences.push(setting);
    }

    /**
     * Saves the unsaved preferences.
     */
    async save(): Promise<void> {
        Log.info("MaterialPreferences saving", {
            unsavedPreferences: this.unsavedPreferences,
        });
        this.unsavedPreferences.forEach((setting) => {
            // find the setting in the configuration
            Log.debug("MaterialPreferences looking for setting", { setting });
            const configurationSet = Object.values(
                Configuration.configurationSets
            ).find((set) => {
                return (
                    Object.values(set).find((s) => s.id === setting.id) != null
                );
            });
            if (configurationSet) {
                Log.debug("MaterialPreferences config set found", {
                    configurationSet,
                });
                const settingToSave = configurationSet[setting.id];
                if (settingToSave) {
                    settingToSave.value = setting.value;
                } else {
                    throw "Setting not found in configuration";
                }
            }
        });
        this.unsavedPreferences = [];
        Log.debug("MaterialPreferences set config items");
        await Configuration.save();
        Log.info("MaterialPreference saved");
        UltravioletUI.Toast.quickShow({
            content: i18next.t<string>("ui:toasts.prefsSaved"),
        });
    }

    render(): HTMLDivElement {
        let config: [key: string, set: Setting<any>[]][] = Object.entries(
            Configuration.configurationSets
        ).map(([key, set]) => [
            key,
            Object.entries(set)
                .map(([, setting]) => setting)
                .filter((setting) => setting.displayInfo != null),
        ]);
        config = config.filter(([, set]) => set.length > 0);
        if (this.props.excludeTabs) {
            config = config.filter(
                ([key]) => !this.props.excludeTabs.includes(key)
            );
        }
        const tabs = config.map(
            ([key, value], index) =>
                new MaterialPreferencesTab({
                    active: index === 0,
                    title: key,
                    items: value,
                    onChange: this.onChange.bind(this),
                })
        );

        return (this.element = (
            <div id="uvPreferences">
                <MaterialTabBar
                    activeTabIndex={0}
                    onActivate={(event) =>
                        tabs.forEach((tab, index) =>
                            index === event.detail.index
                                ? tab.activate()
                                : tab.deactivate()
                        )
                    }
                >
                    {tabs.map((tab) => tab.renderTabBarItem())}
                </MaterialTabBar>
                <div class={"uv-preferences-content"}>
                    {tabs.map((tab) => tab.render())}
                </div>
                <hr />
                <div class={"uv-preferences-save"}>
                    <MaterialButton
                        raised={true}
                        dialogAction={"save"}
                        onClick={() => this.save()}
                    >
                        Save
                    </MaterialButton>
                </div>
            </div>
        ) as HTMLDivElement);
    }
}
