import {
    UltravioletHook,
    UltravioletHookEventTypes,
} from "./UltravioletHookEvent";
import StyleManager from "app/styles/StyleManager";
import Log from "app/data/AppLog";

declare global {
    // noinspection JSUnusedGlobalSymbols
    interface Window {
        UltravioletHooks: {
            [K in UltravioletHookEventTypes]?: UltravioletHook[];
        };
    }
}

/**
 * Ultraviolet Hooks
 *
 * In order to allow for integrations with Ultraviolet from other userscripts,
 * Ultraviolet allows the registration of functions - these so-called
 * "hooks", which are called after specific events during Ultraviolet's execution.
 */
export default class UltravioletHooks {
    static get hooks(): typeof window.UltravioletHooks {
        return window.UltravioletHooks ?? (window.UltravioletHooks = {});
    }

    static assertHookType(hookType: UltravioletHookEventTypes): void {
        if (UltravioletHooks.hooks[hookType] === undefined) {
            UltravioletHooks.hooks[hookType] = [];
        }
    }

    static addHook<T extends UltravioletHookEventTypes>(
        hookType: T,
        hook: UltravioletHook
    ): void {
        Log.trace(`Added hook: ${hookType}`, hook);
        UltravioletHooks.assertHookType(hookType);
        (UltravioletHooks.hooks[hookType] as UltravioletHook[]).push(hook);
    }

    static removeHook<T extends UltravioletHookEventTypes>(
        hookType: T,
        hook: UltravioletHook
    ): void {
        Log.trace(`Removed hook: ${hookType}`, hook);
        UltravioletHooks.assertHookType(hookType);
        (UltravioletHooks.hooks[hookType] as UltravioletHook[]).filter(
            (h) => h !== hook
        );
    }

    static async executeHooks<T extends UltravioletHookEventTypes>(
        hookType: T,
        payload: Record<string, any> = {}
    ): Promise<void> {
        Log.debug(`Executing hook: ${hookType}`);
        UltravioletHooks.assertHookType(hookType);

        if (StyleManager.activeStyle?.hooks?.[hookType])
            for (const hook of StyleManager.activeStyle.hooks[
                hookType
            ] as UltravioletHook[]) {
                const result = hook(payload);
                if (result instanceof Promise) {
                    try {
                        await result;
                    } catch (e) {
                        Log.error(`Hook failed for style: ${hookType}`, {
                            type: hookType,
                            hook: hook,
                            paylod: payload,
                        });
                    }
                }
            }

        for (const hook of UltravioletHooks.hooks[
            hookType
        ] as UltravioletHook[]) {
            const result = hook(payload);
            if (result instanceof Promise) {
                try {
                    await result;
                } catch (e) {
                    Log.error(`Internal hook failed: ${hookType}`, {
                        type: hookType,
                        hook: hook,
                        paylod: payload,
                    });
                }
            }
        }

        document.dispatchEvent(
            new Event(`ultraviolet:${hookType}`, { payload: payload } as Record<
                string,
                any
            >)
        );
    }
}
