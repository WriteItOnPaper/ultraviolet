import { StyleStorage } from "app/styles/Style";
import { Dependency } from "app/data/Dependencies";
import { NamedPage, Page } from "app/mediawiki";

/**
 * <b>UltravioletStore</b> is for live, in-memory data that does not require persistence
 * or is rebuilt on every page load.
 *
 * If you wish to persistently save data,
 * use {@link UltravioletLocalDB} instead. If you wish to create or access a
 * constant value that can be loaded at any point, consider using {@link UltravioletConstants}
 * instead. If you want to generate data for debugging, use {@link Log}
 * instead.
 */
export default class UltravioletStore {
    public static readonly startTime = new Date();

    // Initializations
    public static dependencies: Dependency[] = [
        {
            // Material Icons
            type: "style",
            id: "material-icons",
            // Original: "https://fonts.googleapis.com/icon?family=Material+Icons"
            src: "https://tools-static.wmflabs.org/fontcdn/css?family=Material+Icons",
            cache: {
                delayedReload: true,
                duration: 1209600000, // 14 days
            },
        },
    ];

    // //en.wikipedia.org
    public static wikiBase: string;
    // /wiki/$1
    public static wikiArticlePath: string;
    // //en.wikipedia.org/w/index.php
    public static wikiIndex: string;
    // //en.wikipedia.org/w/api.php
    public static wikiAPI: string;
    // "enwiki"
    public static wikiID: string;
    // URL: "https//en.wikipedia.org/static/images/project-logos/enwiki.png"
    // WARNING: Not calculated on page load. Must be assigned to be `Watch.ts`
    public static wikiLogo: URL;

    public static styleStorage: StyleStorage = null;
    public static windowFocused = false;

    public static currentPage: Page & NamedPage;
    public static get currentNamespaceID(): number {
        return UltravioletStore.currentPage.namespace;
    }
    public static get currentNamespace(): string {
        return UltravioletStore.currentPage.title
            .getNamespacePrefix()
            .replace(/:$/, "");
    }

    public static registerDependency(dependency: Dependency): void {
        UltravioletStore.dependencies.push(dependency);
    }

    public static initializeStore(): void {
        UltravioletStore.wikiArticlePath = mw.config.get(
            "wgArticlePath"
        ) as string;
        UltravioletStore.wikiBase = mw.config.get("wgServer") as string;
        UltravioletStore.wikiIndex =
            (mw.config.get("wgServer") as string) +
            (mw.config.get("wgScript") as string);
        UltravioletStore.wikiAPI = `${
            (mw.config.get("wgServer") as string) +
            (mw.config.get("wgScriptPath") as string)
        }/api.php`;
        UltravioletStore.wikiID = mw.config.get("wgWikiID") as string;
        UltravioletStore.currentPage = Page.fromIDAndTitle(
            mw.config.get("wgArticleId"),
            mw.config.get("wgPageName")
        );

        window.UltravioletStore = UltravioletStore;
    }

    static articlePath(target: string): string {
        return UltravioletStore.wikiArticlePath.replace(
            /\$1/g,
            mw.util.wikiUrlencode(target)
        );
    }

    static getNamespaceId(namespace: string): number | null {
        return mw.config.get("wgNamespaceIds")[
            namespace.replace(/\s/g, "_").toLowerCase()
        ];
    }

    static isUserspacePage(): boolean {
        return Page.isUserspacePage(UltravioletStore.currentPage) !== false;
    }

    static isSpecialPage(): boolean {
        return Page.isSpecialPage(UltravioletStore.currentPage) !== false;
    }
}

// We're exposing Ultraviolet's storage for the ones who want to tinker with
// Ultraviolet's global storage variables. In reality, we don't have to do this,
// but it's more of a courtesy to users who want to experiment a bit more.
declare global {
    // noinspection JSUnusedGlobalSymbols
    interface Window {
        UltravioletStore: typeof UltravioletStore;
    }
}

window.addEventListener("blur", () => {
    UltravioletStore.windowFocused = false;
});

window.addEventListener("focus", () => {
    UltravioletStore.windowFocused = true;
});
