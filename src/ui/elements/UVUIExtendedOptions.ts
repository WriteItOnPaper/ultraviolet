import { UVUIDialog, UVUIDialogProperties } from "app/ui/elements/UVUIDialog";

export interface UVUIExtendedOptionsProperties extends UVUIDialogProperties {
    showDiffIcons?: boolean;
}

export class UVUIExtendedOptions extends UVUIDialog<void> {
    show(): Promise<void> {
        throw new Error("Attempted to call abstract method");
    }
    render(): HTMLDialogElement {
        throw new Error("Attempted to call abstract method");
    }

    public static readonly elementName = "uvExtendedOptions";

    constructor(readonly props: UVUIExtendedOptionsProperties = {}) {
        super(props);
    }
}
